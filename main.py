import tensorflow as tf
from tensorflow.keras.datasets import mnist
from keras.callbacks import ModelCheckpoint
from tensorflow.keras.layers import Conv2D, MaxPool2D, Dropout, Flatten, Dense

# Загрузка обучающей и тестовой выборок. X - изображения цифр, Y - вектор соответствующих значений цифр (меток класса)
(x_train, y_train), (x_test, y_test) = mnist.load_data()

# Построение архитектуры модели нейросети.
# По данным с ресурса Kaggle, такая архитектура является одной из самых эффективных для данной задачи.
# Экмпериментально было установлено, что оптимальным входным значением для слоев Dropout является 0.2.
model = tf.keras.Sequential()
model.add(Conv2D(32, kernel_size=5, activation='relu', input_shape=(28, 28, 1)))
model.add(MaxPool2D())
model.add(Dropout(.2))
model.add(Conv2D(64, kernel_size=5, activation='relu'))
model.add(MaxPool2D())
model.add(Dropout(.2))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(.2))
model.add(Dense(10, activation='softmax'))

print(model.summary())

# Приведение значений векторов X к диапазону от 0 до 1 для упрощения вычислений
x_train = x_train / 255
x_test = x_test / 255

# Преобразование вектора с метками классов к бинарной матрице соответствия классам
y_train_cat = tf.keras.utils.to_categorical(y_train, 10)
y_test_cat = tf.keras.utils.to_categorical(y_test, 10)

# Настройка модели для обучения
model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

# Создание чекпоинта для сохранения в файл весов с лучшими показателями метрики "accuracy" на валидационных данных
weights_file = "weights.h5"
checkpoint = ModelCheckpoint(weights_file, monitor='val_accuracy', mode='max', save_best_only=True, verbose=1)

# Обучение модели.
# Количество эпох может быть уменьшено или увеличено, однако
# в первом случае вероятны потери в точности, а во втором не наблюдается значительных улучшений
model.fit(x_train, y_train_cat, callbacks=[checkpoint], batch_size=32, epochs=20, validation_split=0.2)

# Загрузка "лучших" весов из файла "weights.h5"
model.load_weights(weights_file)

# Вывод в консоль значений метрик "loss" и "accuracy" на тестовых данных
print(model.evaluate(x_test, y_test_cat))

